/*
 * Name of the Class : Loader.Java
 * Author            : Shreyas S Kulkarni
 * Purpose           : Visits all the files using FileVisitor and indexes the files 
 * 					   On a local datastore(SQLite database)
 * Reusage           : Retain the functions provided by FileVisitor, rest can be modified as
 * 					   per requirement
 */

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.*;
import java.sql.SQLException;

import static java.nio.file.FileVisitResult.*;
import static java.nio.file.FileVisitOption.*;

public class Loader extends SimpleFileVisitor<Path> {


	dbOperationClass dbOperations = null;

	int driveId;
	int failedFiles;
	int indexedFiles;


	public Loader() throws ClassNotFoundException, SQLException
	{
		dbOperations = new dbOperationClass();
		failedFiles = 0;
		driveId = -1;
		indexedFiles =0;
	}
	public int getIndexedFileCount()
	{
		return indexedFiles;
	}
	public void addFailedFiles()
	{
		this.failedFiles++;
	}
	public int getFailedFiles()
	{
		return this.failedFiles;
	}
	public void setDriveId(int dId)
	{
		this.driveId = dId;
	}
	public int getDriveId()
	{
		return this.driveId;
	}
	public void setFailedFiles(int failedFiles)
	{
		this.failedFiles = failedFiles;
	}
	public void setIndexedFileCount(int indexedFileCount)
	{
		this.indexedFiles = indexedFileCount;
	}


	//This function is invoked when a file is visited.
	@Override
	public FileVisitResult visitFile(Path file,
			BasicFileAttributes attrs) {

		indexedFiles++;
		java.util.Date current = new java.util.Date();


		// preparing queries using prepared statement

		try {
			dbOperations.psIndexMain.setInt(1, getDriveId());
			dbOperations.psIndexMain.setString(2, file.getFileName().toString());
			dbOperations.psIndexMain.setString(3, file.toString());
			dbOperations.psIndexMain.setInt(4, 0);
			dbOperations.psIndexMain.setString(5, (new java.sql.Timestamp(current.getTime())).toString());
			dbOperations.psIndexMain.addBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}


		return CONTINUE;
	}

	//this function is invoked before any Directory is visited
	@Override
	public FileVisitResult preVisitDirectory(Path dir,
			BasicFileAttributes attrs) {

		indexedFiles++;
		java.util.Date current = new java.util.Date();

		//use prepared statement to generate query

		try {
			dbOperations.psIndexMain.setInt(1, getDriveId());
			dbOperations.psIndexMain.setString(2,dir.getFileName().toString());
			dbOperations.psIndexMain.setString(3, dir.toString());
			dbOperations.psIndexMain.setInt(4, 1);
			dbOperations.psIndexMain.setString(5, (new java.sql.Timestamp(current.getTime())).toString());


			// all queries to be executed in batch to enhance performance
			dbOperations.psIndexMain.addBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (NullPointerException e) {
		}


		return CONTINUE;
	}

	//this function is invoked when there is a failure in visiting a file
	@Override
	public FileVisitResult visitFileFailed(Path file,
			IOException exc) {
		System.out.println(file + " Couldnt be indexed because Access was denied");
		addFailedFiles();
		return CONTINUE;
	}

}
