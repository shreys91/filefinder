/*
 * Name of the Class : dbOperationClass.Java
 * Author            : Shreyas S Kulkarni
 * Purpose           : handles database operations, manages connections 
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class dbOperationClass
{

	public final String databaseUrl = "jdbc:sqlite:database.db";

	public Connection connection = null;
	public String insertIntoIndexMain = null;  
	public PreparedStatement psIndexMain = null;
	public Statement statement = null;

	public dbOperationClass() throws ClassNotFoundException, SQLException
	{
		Class.forName("org.sqlite.JDBC");
		connection = DriverManager.getConnection(databaseUrl);
		statement = connection.createStatement();
		statement.setQueryTimeout(120);  // set timeout to 2 mins.
		insertIntoIndexMain = "insert into index_main(drive_id, file_nm , file_path,directory, index_AT) values ( ?,?,?,?,?)";
		psIndexMain = connection.prepareStatement(insertIntoIndexMain);

	}
	public boolean executeQuery(String query)
	{

		try 
		{
			statement.execute(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}


}
