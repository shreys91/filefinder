/*
 * Name of the Class : FileFinder.Java
 * Author            : Shreyas S Kulkarni
 * Purpose           : This is the starting point of the application. 
 * Application       : FileFinder : Indexing files and searching them.
 */


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FileFinder {

	//Command String Format
	public static void displayFormat()
	{
		System.out.println("Please use the following format : FileFinder --mode arguments");
		System.out.println("--mode [load/find]");
		System.out.println("arguments : when mode:load Directories, when mode:find Files or Directories");
	}

	//Starting of an Application
	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException

	{

		Loader loader = new Loader();
		String mode=null;
		Path startingDir = null;

		if(args.length < 2)
		{
			displayFormat();
			return;
		}
		mode = args[0];


		if(mode.equals("--load"))
		{
			if(args.length <2)
				displayFormat();
			else
			{
				for(int i=1; i<args.length;i++)
				{
					System.out.println("Directory : " + args[i]);
					loader.setFailedFiles(0);
					loader.setIndexedFileCount(0);

					boolean prevIndexed;

					//check valid directory
					prevIndexed = checkIfDriveIndexed(args[i]);
					if(!prevIndexed)
					{

						System.out.println("Indexing......");
						//if the drive is being indexed for the first time, an entry is made
						//in the master table for future references and re-indexing needs.
						addEntryinMasterTable(args[i]);
						loader.setDriveId(getDriveId(args[i]));


						// File tree traversal begins.
						startingDir = Paths.get(args[i]);
						Files.walkFileTree(startingDir, loader);

						//saving of data in datastore in batch.
						loader.dbOperations.connection.setAutoCommit(false);
						System.out.println("Saving data in local datastore.....");
						loader.dbOperations.psIndexMain.executeBatch();
						loader.dbOperations.connection.commit();

						//display of operation results. 
						System.out.println("Indexing of " + args[i] + " complete.");
						System.out.println("Total FIles indexed: " + (loader.getIndexedFileCount() + loader.getFailedFiles()));
						System.out.println("Sucessfully Indexed files: " + loader.getIndexedFileCount());
						System.out.println("Failed Files:"+ loader.getFailedFiles());

					}
					else
					{
						//If drive is already indexed, the existing index is deleted and then new
						//data is stored in datastore. 
						System.out.println("Reindexing....");
						deleteFromIndex(getDriveId(args[i]));

						//Begin of file tree traversal
						startingDir = Paths.get(args[i]);
						Files.walkFileTree(startingDir, loader);

						//Saving to database in batch
						loader.dbOperations.connection.setAutoCommit(false);
						System.out.println("Saving data in local datastore.....");
						loader.dbOperations.psIndexMain.executeBatch();
						loader.dbOperations.connection.commit();

						//Output Operation Results
						System.out.println("Indexing of " + args[i] + " complete.");
						System.out.println("Total FIles indexed: " + (loader.getIndexedFileCount() + loader.getFailedFiles()));
						System.out.println("Sucessfully Indexed files: " + loader.getIndexedFileCount());
						System.out.println("Failed Files:"+ loader.getFailedFiles());
					}

				}
			}
		}

		else if(mode.equals("--find"))
		{
			if(args.length <2)
				displayFormat();
			else
			{
				String fileToFind = args[1];
				searchanddisplayResults(fileToFind);

			}

		}
		else
		{
			displayFormat();
		}

	}

	private static void searchanddisplayResults(String fileToFind) throws ClassNotFoundException, SQLException {

		dbOperationClass dbOps = new dbOperationClass();
		String query;
		query = "select * from index_main where file_nm = \"";
		query = query + fileToFind;
		query = query + ("\"");
		ResultSet rs = dbOps.statement.executeQuery(query);
		int numResults=0;
		System.out.println("SEARCH RESULTS");
		while(rs.next())
		{
			numResults++;
			System.out.println("_____________________________________________");
			String file_nm = rs.getString(3);
			String file_path = rs.getString(4);
			String type;
			if(rs.getInt(5) == 0)
			{
				type = "File";
			}
			else
			{
				type = "Directory";

			}
			String PreviouslyIndexed = rs.getString(6);


			System.out.println("FILE: " + file_nm );
			System.out.println("PATH: " + file_path);
			System.out.println("Type: " + type);
			System.out.println("Last Indexed On: "+ PreviouslyIndexed);

			System.out.println("_____________________________________________");
		}
		System.out.println("Total " + numResults + " results found.");
	}

	
	private static void deleteFromIndex(int driveId) throws ClassNotFoundException, SQLException {

		dbOperationClass db = new dbOperationClass();  
		String query;
		query = "delete from index_main where drive_id =";
		query = query + driveId;
		db.executeQuery(query);
		db.connection.close();

	}
	private static void addEntryinMasterTable(String drive) throws ClassNotFoundException, SQLException {
		dbOperationClass db = new dbOperationClass();
		java.util.Date current = new java.util.Date();
		String query;
		query = "insert into drive_indexed(DRIVE_PATH,INDEXED_ON) values (\"";
		query = query + drive;
		query = query + ("\\\",\"");
		query = query + (new java.sql.Timestamp(current.getTime()));
		query = query + "\")";
		db.executeQuery(query);
		db.connection.close();

	}
	private static int getDriveId(String drive) throws SQLException, ClassNotFoundException
	{
		int driveId;
		dbOperationClass dbOps = new dbOperationClass();
		String query;
		query = "select drive_id from drive_indexed where drive_path = \"";
		query = query + drive;
		query = query + ("\\\"");
		ResultSet rs = dbOps.statement.executeQuery(query);
		driveId = rs.getInt(1);
		dbOps.connection.close();
		return driveId;


	}
	private static boolean checkIfDriveIndexed(String drive) throws SQLException, ClassNotFoundException
	{
		dbOperationClass dbOps = new dbOperationClass();
		String query;
		query = "select drive_id from drive_indexed where drive_path = \"";
		query = query + drive;
		query = query + ("\\\"");

		ResultSet rs = dbOps.statement.executeQuery(query);
		dbOps.connection.close();
		if(!rs.next())
			return false;
		else
			return true;

	}

}

